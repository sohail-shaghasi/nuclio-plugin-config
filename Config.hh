<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\config
{
	require_once('types.hh');
	
	use nuclio\core\
	{
		plugin\Plugin,
		ClassManager
	};
	use nuclio\plugin\
	{
		fileSystem\reader\FileReader,
		fileSystem\folder\Folder,
		format\driver\neon\Neon,
		format\driver\yaml\YAML,
		config\ConfigException
	};
	use Nette\Neon\Entity;
	use nuclio\helper\CryptHelper;
	
	/**
	 * Auth Class
	 * Config
	 * @factory
	 */
	<<factory>>
	class Config extends Plugin
	{
		private ConfigCollection $config;
		/**
	     * call the constructor of the config class.
	     */
		public static function getInstance(...$args):Config
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		/**
	     * call the constructor of the auth class.
	     * look for the config file accepts file type neon, yml or yaml
	     * @param string refers to the configuration file path.
	     */
		public function __construct(string $configPath)
		{
			parent::__construct();
			$config		=Map{};
			$allConfig	=[];
			$configPath	=rtrim($configPath,'/').'/';
			$folder		=Folder::getInstance('fileSystem::local-disk',$configPath);
			$configFiles=$folder->listAllFilesAndFolders();
			
			foreach($configFiles as $configFile)
			{
				if ($configFile!='.'
				&& $configFile!='..'
				&& substr($configFile,-1,1)!='~')
				{
					$filePath	=$configPath.$configFile;
					$file		=FileReader::getInstance('fileSystem::local-disk',$filePath);
					$ext		=$file->getExt();
					if(!is_null($ext))
					{
						if($ext=='neon')
						{
							$config=Neon::decode($filePath);
						}
						elseif($ext=='yaml' || $ext=='yml')
						{
							$config=YAML::parse($filePath);
						}
						else
						{
							$config=new Map(explode('',$filePath));
						}
					}
					else
					{
						throw new ConfigException('Wrong file extension!');
					}
					$allConfig=array_merge($allConfig,$config->toArray());
				}
			}
			$this->config=new Map($allConfig);
		}
		
		/**
	     * Get return a specific config in the config file depending on a passed keyword.
	     * @param  string for the keyword in the config.
	     * @return mixed  get a specific configuration from the config list.
	     */
		public function get(string $configKey):mixed
		{
			$nodes=new Vector(explode('.',$configKey));
			
			$currentVariable=$this->config;
			for ($i=0,$j=$nodes->count(); $i<$j; $i++)
			{
				if (is_null($currentVariable))
				{
					continue;
				}
				if ($currentVariable instanceof Traversable)
				{
					if ($this->containsKey($currentVariable,$nodes[$i]))
					{
						$currentVariable=$currentVariable->get($nodes[$i]);
						if (is_array($currentVariable))
						{
							$toConvert=$currentVariable;
							// try
							// {
							// 	$currentVariable=new Vector($conversion);
							// }
							// catch (\InvalidArgumentException $exception)
							// {
							// 	$currentVariable=new Map($conversion);
							// }
							$keys=array_keys($currentVariable);
							$vector=true;
							for ($k=0,$l=count($keys); $k<$l; $k++)
							{
								if (is_string($keys[$k]))
								{
									$vector=false;
									break;
								}
							}
							if ($vector)
							{
								$currentVariable=new Vector($toConvert);
							}
							else
							{
								$currentVariable=new Map($toConvert);
							}
						}
						else
						{
							break;
						}
					}
					else
					{
						return null;
					}
				}
				else
				{
					break;
				}
			}
			
			return $this->recursiveDecrypt($currentVariable);
		}
		
		private function recursiveDecrypt(mixed $currentVariable):mixed
		{
			if (!is_scalar($currentVariable))
			{
				if ($currentVariable instanceof Entity
				&& $currentVariable->value=='decrypt')
				{
					$crypt=CryptHelper::getInstance();
					$currentVariable=$crypt->decrypt($currentVariable->attributes[0]);
				}
				else if (is_array($currentVariable)
				|| $currentVariable instanceof Map
				|| $currentVariable instanceof Vector)
				{
					foreach ($currentVariable as $key=>$val)
					{
						$currentVariable[$key]=$this->recursiveDecrypt($val);
					}
				}
			}
			return $currentVariable;
		}
		
		/**
	     * Check if the keywrod excist in the config.
	     * @param  mixed currentVariable get the config as a victor or map.
	     * @param  mixed check if the key excist in the list.
	     */
		private function containsKey(mixed $currentVariable,mixed $key):bool
		{
			if ($currentVariable instanceof Vector)
			{
				$key=(int)$key;
				return $currentVariable->containsKey($key);
			}
			else if ($currentVariable instanceof Map)
			{
				$key=(string)$key;
				return $currentVariable->containsKey($key);
			}
			return false;
		}
		
		/**
		 * Read the config file.
		 * @return Map<string,mixed> The config from the config file.
		 */
		public function getConfig():ConfigCollection
		{
			return $this->config;
		}

		/**
		 * Read the specific key from the config file.
		 * @param  string $key The key name that we want to read from config file.
		 * @return mixed      The value from the given key
		 */
		public function getKey(string $key):mixed
		{
			if($this->config->containsKey($key))
			{
				return $this->config->get($key);
			}
			throw new ConfigException('key does not exist!');
		}

		/**
		 * Set the key with a given value, create the key if does not exist, update it if exist.
		 * @param string $key   What to be set.
		 * @param string $value Value that want to be set.
		 */
		public function setKey(string $key, string $value):void
		{
			$this->config[$key]=$value;
		}
	}
}
